# **ASPIRE**
#### **The Aspiration Abyss**

### **Table Of Content**

* [Development](#development)


### **Development**

To run this API in development mode use image (from Container Registry) with `build` suffix
e.g: `v0.1.0-build` in your `docker-compose.yml` file.

In `api` container `image` should be changed to e.g.:

`image: registry.gitlab.com/self-evolving/aspire/aspire-server:v0.1.0-build`
