import time

from psycopg2 import OperationalError as Psycopg20pError
from django.db.utils import OperationalError
from django.core.management.base import BaseCommand

DB_CONN_TIMEOUT = 120


class Command(BaseCommand):

    def handle(self, *args, **options):
        self.stdout.write("Waiting for database...")
        db_available = False
        start_time = time.time()

        while not db_available and time.time() - start_time < DB_CONN_TIMEOUT:
            try:
                self.check(databases=['default'])
                db_available = True
                self.stdout.write(self.style.SUCCESS("Database available!"))
            except (Psycopg20pError, OperationalError):
                self.stdout.write("Database unavailable, retry...")
                time.sleep(1)

        if not db_available:
            self.stdout.write(
                self.style.ERROR(
                    f"Timeout ({DB_CONN_TIMEOUT} seconds) reached. Database still unavailable."
                )
            )
