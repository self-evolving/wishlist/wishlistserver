from django.core.mail import EmailMultiAlternatives
from django.template.loader import render_to_string
from django.conf import settings


def send_email(
        user,
        html_msg_name: str,
        email_subject: str,
        extra_context: dict = None
):
    context = {
        'current_user': user,
        'email': user.email,
    }

    if extra_context is not None:
        context.update(extra_context)

    html_message = render_to_string(f"email/{html_msg_name}", context)

    msg = EmailMultiAlternatives(
        subject=email_subject,
        body=html_message,
        from_email=settings.EMAIL_HOST_USER,
        to=[user.email],
    )

    msg.attach_alternative(html_message, "text/html")
    msg.send()
