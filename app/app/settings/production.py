import environ

from .base import *

env = environ.Env()
environ.Env.read_env()

DEBUG = False

ALLOWED_HOSTS = [".andret.eu", "api.wishlist.andret.eu"]


DATABASES = {
    "default": {
        "ENGINE": env("DB_ENGINE", default="django.db.backends.postgresql"),
        "NAME": env("DB_NAME", default="aspireDB"),
        "USER": env("DB_USER", default="postgres"),
        "PASSWORD": env("DB_PASSWORD", default="Testowe123!"),
        "HOST": env("DB_HOST", default="postgres"),
        "PORT": env("DB_PORT", default="5432"),
    }
}


# CORS

CORS_ALLOWED_ORIGINS = ["https://wishlist.andret.eu"]
CORS_ALLOW_CREDENTIALS = True


# security

# TODO: Infinity redirection bug
# SSL redirection, require use HTTPS
# SECURE_SSL_REDIRECT = True

# cookies related settings
SESSION_COOKIE_SECURE = True
CSRF_COOKIE_SECURE = True

# protect against XSS attacks
SECURE_BROWSER_XSS_FILTER = True
