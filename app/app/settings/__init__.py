import os

from environ import Env

env = Env()
env.read_env()

DJANGO_SETTINGS_MODULE = os.environ.get('DJANGO_SETTINGS_MODULE')

if DJANGO_SETTINGS_MODULE == 'app.settings.production':
    from .production import *
else:
    from .development import *
