import os

from environ import Env

from django.core.wsgi import get_wsgi_application


env = Env()
env.read_env()

DJANGO_SETTINGS_MODULE = os.environ.get('DJANGO_SETTINGS_MODULE', 'app.settings.development')
os.environ.setdefault('DJANGO_SETTINGS_MODULE', DJANGO_SETTINGS_MODULE)

application = get_wsgi_application()
