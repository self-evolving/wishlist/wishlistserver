import uuid

from django.db import models
from django.conf import settings
from django.core.validators import MaxValueValidator, MinValueValidator
from django.contrib.auth.hashers import make_password, check_password


class WishList(models.Model):
    """Model representing a wishlist"""

    class Meta:
        db_table = "wishlists"

    uuid = models.UUIDField(default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=255)
    is_deleted = models.DateTimeField(null=True, blank=True)
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    access_code = models.CharField(max_length=255, blank=True, null=True)
    has_password = models.BooleanField(default=False)

    def __str__(self):
        return self.name

    def set_access_code(self, raw_password: str) -> None:
        """
        Set a hashed access code for the hidden items of a wishlist

        :param raw_password: The raw access code to hash and store
        """
        self.access_code = make_password(raw_password)
        self.update_has_password()

    def check_access_code(self, raw_password: str) -> bool:
        """
        Verify whether the given raw access code matches the stored hashed access code

        :param raw_password: The raw access code to verify

        :return bool: True/False
        """
        return check_password(raw_password, self.access_code)

    def update_has_password(self) -> None:
        """
        Update the `has_hidden_items` flag based on the presence of hidden items.

        This method checks whether any associated wishlist items are marked as hidden
        and updates the `has_hidden_items` field accordingly.
        """
        self.has_password = bool(self.access_code)
        self.save()


class WishListItem(models.Model):
    """Model representing an item within a wishlist"""

    class Meta:
        db_table = "wishlist_items"

    name = models.CharField(max_length=255)
    description = models.TextField()
    priority_id = models.IntegerField(
        default=1, validators=[MaxValueValidator(5), MinValueValidator(1)]
    )
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    wishlist = models.ForeignKey(
        WishList,
        on_delete=models.DO_NOTHING,
    )
    hidden = models.BooleanField(default=False)

    def __str__(self):
        return self.name
