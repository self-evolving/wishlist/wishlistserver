from django.urls import path, include
from rest_framework.routers import DefaultRouter

from . import views


router = DefaultRouter(trailing_slash=False)
router.register('wishlist', views.WishListViewSet)
router.register(r'(?P<wishlist_id>\d+)/wishlistitem', views.WishListItemsViewSet)

app_name = 'wishlist'

urlpatterns = [
    path('', include(router.urls)),
]
