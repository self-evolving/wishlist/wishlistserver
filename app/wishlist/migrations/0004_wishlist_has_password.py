# Generated by Django 4.2.8 on 2025-02-03 20:56

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("wishlist", "0003_wishlist_access_code_wishlistitem_hidden"),
    ]

    operations = [
        migrations.AddField(
            model_name="wishlist",
            name="has_password",
            field=models.BooleanField(default=False),
        ),
    ]
