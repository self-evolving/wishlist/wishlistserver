from rest_framework import serializers
from rest_framework.utils.serializer_helpers import ReturnList

from .models import WishList, WishListItem


class WishListItemSerializer(serializers.ModelSerializer):
    """Serializer for the WishListItem model"""

    class Meta:
        model = WishListItem
        fields = ["id", "name", "description", "priority_id", "hidden"]
        read_only_fields = ["id"]


class WishListDetailedSerializer(serializers.ModelSerializer):
    """
    Serializer for the WishList model that includes detailed information
    about associated wishlist items
    """

    wishlist_items = serializers.SerializerMethodField()

    class Meta:
        model = WishList
        fields = [
            "id",
            "uuid",
            "name",
            "wishlist_items",
            "access_code",
            "has_password",
        ]
        read_only_fields = ["id", "uuid", "has_password"]
        extra_kwargs = {"access_code": {"write_only": True}}

    @staticmethod
    def get_wishlist_items(obj: WishList) -> ReturnList:
        """
        Retrieve a list of visible wishlist items for the given wishlist.

        This method filters the associated wishlist items to return
        only those that are not marked as hidden.

        :param obj: The wishlist instance from which to retrieve items.

        :return: A serialized list of visible wishlist items.
        """
        items = obj.wishlistitem_set.filter(hidden=False)
        serializer = WishListItemSerializer(items, many=True)
        return serializer.data


class WishListSerializer(serializers.ModelSerializer):
    """
    Serializer for the WishList model
    """

    class Meta:
        model = WishList
        fields = ["id", "uuid", "name", "access_code"]
        read_only_fields = ["id", "uuid"]
        extra_kwargs = {"access_code": {"write_only": True}}


class WishListAccessCodeSerializer(serializers.Serializer):
    """Serializer for handling access code for wishlists"""

    access_code = serializers.JSONField(required=True)
