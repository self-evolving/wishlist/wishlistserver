"""Tests for WishList related endpoints"""
from django.test import TestCase
from django.urls import reverse
from django.contrib.auth import get_user_model
from rest_framework.test import APIClient
from rest_framework import status

from wishlist.models import WishList
from wishlist.serializers import WishListDetailedSerializer

WISHLIST_URL = reverse('wishlist:wishlist-list')


def create_user(**params):
    return get_user_model().objects.create_user(**params)


def create_wishlist(user) -> WishList:
    payload = {
        'name': 'Sample wishlist name',
    }

    wishlist = WishList.objects.create(user=user, **payload)

    return wishlist


def get_url_with_id(wishlist_id: int) -> str:
    return reverse('wishlist:wishlist-detail', args=[wishlist_id])


class PublicWishListApiTests(TestCase):
    """Tests for unauth API requests"""

    def setUp(self) -> None:
        self.client = APIClient()

    def test_auth_required(self):
        """Test if error occure when user do not authorized"""
        res = self.client.get(WISHLIST_URL)

        self.assertEqual(res.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_get_wishlist_by_uuid(self):
        """Test if request return wishlist by uuid without auth"""
        user = create_user(email='test@example.com', password='Testowe123!')
        self.client.force_authenticate()
        wishlist = create_wishlist(user=user)
        self.client.logout()

        url = reverse('wishlist:wishlist-get-wishlist-by-uuid', kwargs={'uuid': wishlist.uuid})

        res = self.client.get(url)

        serializer = WishListDetailedSerializer(wishlist)

        self.assertEqual(res.status_code, status.HTTP_200_OK)
        self.assertEqual(res.data, serializer.data)


class PrivateWishListApiTests(TestCase):
    """Tests for auth API requests"""

    def setUp(self) -> None:
        self.client = APIClient()
        self.user = create_user(email='test@example.com', password='Testowe123!')
        self.client.force_authenticate(self.user)

    """GET ENDPOINTS"""
    def test_retrieve_wishlists(self):
        """Test if request return users wishlists"""
        create_wishlist(user=self.user)
        create_wishlist(user=self.user)

        res = self.client.get(WISHLIST_URL)

        wishlists = WishList.objects.filter(user=self.user)
        serializer = WishListDetailedSerializer(wishlists, many=True)

        self.assertEqual(res.status_code, status.HTTP_200_OK)
        self.assertEqual(res.data, serializer.data)

    def test_wishlist_list_limited_to_user(self):
        """Test if request return wishlist lists limited to auth user"""
        sec_user = create_user(email='test2@example.com', password='Testowe123!')

        create_wishlist(user=self.user)
        create_wishlist(user=sec_user)

        res = self.client.get(WISHLIST_URL)

        wishlists = WishList.objects.filter(user=self.user)
        serializer = WishListDetailedSerializer(wishlists, many=True)

        self.assertEqual(res.status_code, status.HTTP_200_OK)
        self.assertEqual(res.data, serializer.data)

    def test_get_wishlist_by_id(self):
        """Test if request return wishlist by id"""
        wishlist = create_wishlist(user=self.user)

        url = get_url_with_id(wishlist.id)
        res = self.client.get(url)
        serializer = WishListDetailedSerializer(wishlist)

        self.assertEqual(res.status_code, status.HTTP_200_OK)
        self.assertEqual(res.data, serializer.data)

    """CREATE ENDPOINT"""
    def test_create_wishlist(self):
        """Test creating a wishlist"""
        payload = {
            "name": "Sample name",
        }

        res = self.client.post(WISHLIST_URL, payload)

        self.assertEqual(res.status_code, status.HTTP_201_CREATED)

        wishlist = WishList.objects.get(id=res.data['id'])
        self.assertEqual(res.data['name'], wishlist.name)
        self.assertEqual(self.user, wishlist.user)

    """UPDATE ENDPOINT"""
    def test_update_wishlist(self):
        """Test update of a wishlist"""
        wishlist = create_wishlist(user=self.user)

        new_payload = {
            'name': "UPDATED name",
        }

        url = get_url_with_id(wishlist.id)
        res = self.client.put(url, new_payload)

        self.assertEqual(res.status_code, status.HTTP_200_OK)
        wishlist.refresh_from_db()
        self.assertEqual(new_payload['name'], res.data['name'])
        self.assertEqual(self.user, wishlist.user)

    """DELETE ENDPOINT"""
    def test_delete_wishlist(self):
        """Test delete a wishlist"""
        wishlist = create_wishlist(user=self.user)

        url = get_url_with_id(wishlist.id)
        res = self.client.delete(url)

        self.assertEqual(res.status_code, status.HTTP_200_OK)
        self.assertTrue(WishList.objects.filter(id=wishlist.id))
