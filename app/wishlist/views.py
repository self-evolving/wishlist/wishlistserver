from datetime import datetime
from typing import Any, Optional

from django.shortcuts import get_object_or_404
from rest_framework import viewsets, mixins
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework.permissions import AllowAny, IsAuthenticated

from .models import WishList, WishListItem
from . import serializers


class WishListViewSet(viewsets.ModelViewSet):
    serializer_class = serializers.WishListDetailedSerializer
    queryset = WishList.objects.filter(is_deleted=None)
    permission_classes = [IsAuthenticated]
    http_method_names = ["get", "post", "put", "delete"]

    def perform_create(
        self, serializer: serializers.WishListDetailedSerializer
    ) -> None:
        """
        Override the perform_create method to save the user
        associated with the wishlist.

        :param serializer: The serializer instance containing the wishlist data.
        """
        serializer.save(user=self.request.user)

    def get_serializer_class(self) -> type:
        """
        Determine the serializer class to use based on the action.

        :return type: A serializer class that corresponds to the current action.
        """
        serializer_map = {
            "list": serializers.WishListDetailedSerializer,
            "retrieve": serializers.WishListDetailedSerializer,
            "update": serializers.WishListSerializer,
            "partial_update": serializers.WishListSerializer,
            "destroy": serializers.WishListSerializer,
            "create": serializers.WishListSerializer,
            "set_access_code": serializers.WishListAccessCodeSerializer,
        }
        return serializer_map.get(self.action)

    def get_queryset(self) -> Any:
        """
        Retrieve the queryset of wishlists belonging to the authenticated user.

        :return Any: A queryset of wishlists filtered by the authenticated user,
            ordered by their ID.
        """
        return self.queryset.filter(user=self.request.user).order_by("id")

    @action(
        detail=False,
        methods=["GET"],
        url_path="by_uuid/(?P<uuid>[^/.]+)",
        permission_classes=[AllowAny],
    )
    def get_wishlist_by_uuid(self, _, uuid: str = None) -> Response:
        """
        Retrieve a wishlist by its UUID.

        :param uuid: The UUID of the wishlist to retrieve.

        :return: Response containing the wishlist data if found,
                 or a 404 error message if not found.
        """
        try:
            wishlist = self.queryset.get(uuid=uuid)
            serializer = self.serializer_class(wishlist)
            return Response(serializer.data)
        except WishList.DoesNotExist:
            return Response(
                {"detail": "Wishlist with the specified UUID not found."}, status=404
            )

    def destroy(self, request: Any, *args, **kwargs) -> Response:
        """
        Soft delete a wishlist by marking it as deleted.

        :param request: The request object containing the user making the request.

        :return: Response indicating the status of the deletion.
        """
        wishlist = get_object_or_404(
            WishList, pk=self.kwargs.get("pk"), user=request.user
        )

        if wishlist.is_deleted:
            return Response({"detail": "Not found"}, status=404)

        wishlist.is_deleted = datetime.now()
        wishlist.save()

        return Response({"detail": "Object deleted"}, status=200)

    @action(detail=True, methods=["POST"])
    def set_access_code(self, request: Any, pk: Optional[int] = None) -> Response:
        """
        Set an access code for the specified wishlist.

        :param request: The request object containing the access code data.
        :param pk: The primary key of the wishlist to set the access code for.

        :return: Response indicating the success or failure of setting the access code.
        """
        wishlist = self.get_object()
        access_code = request.data.get("access_code")

        if access_code:
            wishlist.set_access_code(access_code)
            wishlist.save()
            return Response({"message": "Access code set successfully"}, status=200)
        return Response({"message": "Access code required"}, status=400)


class WishListItemsViewSet(
    mixins.CreateModelMixin,
    mixins.RetrieveModelMixin,
    mixins.UpdateModelMixin,
    mixins.DestroyModelMixin,
    mixins.ListModelMixin,
    viewsets.GenericViewSet,
):

    serializer_class = serializers.WishListItemSerializer
    queryset = WishListItem.objects.all()
    permission_classes = [IsAuthenticated]
    http_method_names = ["get", "post", "put", "delete"]

    def perform_create(self, serializer: serializers.WishListItemSerializer) -> None:
        """
        Override the perform_create method to associate the item
        with the specific wishlist and user.

        :param serializer: The serializer instance containing the item data.
        """
        wishlist_id = self.kwargs.get("wishlist_id")
        wishlist = get_object_or_404(
            WishList, pk=wishlist_id, is_deleted=None, user=self.request.user
        )
        serializer.save(wishlist=wishlist, user=self.request.user)

    def get_queryset(self) -> Any:
        """Regular GET - just unhidden items"""
        queryset = super().get_queryset()
        wishlist_id = self.kwargs.get("wishlist_id")
        if wishlist_id:
            get_object_or_404(
                WishList, pk=wishlist_id, is_deleted=None, user=self.request.user
            )
            if self.action == "list":
                return queryset.filter(wishlist_id=wishlist_id, hidden=False)
            return queryset.filter(wishlist_id=wishlist_id)
        return queryset

    @action(detail=False, methods=["GET"], url_path="hidden_items")
    def get_hidden_items(self, request, wishlist_id: Optional[int] = None) -> Response:
        """
        Retrieve hidden items from a specific wishlist.

        :param request: The request object.
        :param wishlist_id: The ID of the wishlist from which to retrieve hidden items.

        :return: Response containing the hidden items data if found,
                 or an error message if the wishlist is not found
                 or access is denied.
        """
        if not wishlist_id:
            return Response({"detail": "Wishlist ID is required."}, status=400)

        wishlist = get_object_or_404(
            WishList, pk=wishlist_id, is_deleted=None, user=self.request.user
        )

        access_code = request.headers.get("Access-Code")

        if not access_code or not wishlist.check_access_code(access_code):
            return Response({"detail": "Invalid or missing access code."}, status=400)

        hidden_items = WishListItem.objects.filter(wishlist=wishlist, hidden=True)
        serializer = self.get_serializer(hidden_items, many=True)
        return Response(serializer.data)
