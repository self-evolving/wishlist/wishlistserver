from django.dispatch import receiver

from django_rest_passwordreset.signals import reset_password_token_created, post_password_reset

from core.common.utils import send_email


@receiver(reset_password_token_created)
def password_reset_token_created(sender, instance, reset_password_token, *args, **kwargs):
    base_reset_url = instance.request.data.get('url')

    context = {
        'reset_password_url': f"{base_reset_url}/{reset_password_token.key}"
    }

    send_email(
        user=reset_password_token.user,
        html_msg_name="password_reset_email.html",
        email_subject="Reset your Aspire password",
        extra_context=context,
    )


@receiver(post_password_reset)
def handle_post_password_reset(sender, user, *args, **kwargs):

    send_email(
        user=user,
        html_msg_name="password_reset_successful_confirmation.html",
        email_subject="Password reset successfully",
    )
