from django.urls import path
from rest_framework_simplejwt.views import TokenRefreshView

from .views import (
    UserView,
    AuthTokenView,
    CustomResetPasswordView,
    ChangePasswordView,
    CustomResetPasswordConfirmView
)


app_name = 'users'


urlpatterns = [
    path('register', UserView.as_view(), name='register_user'),
    path('login', AuthTokenView.as_view(), name='login_user'),
    path('login/refresh', TokenRefreshView.as_view(), name='token_refresh'),
    path('password_reset', CustomResetPasswordView.as_view(), name='password_reset'),
    path('password_reset/confirm', CustomResetPasswordConfirmView.as_view(), name='password_reset_confirm'),
    path('change_password', ChangePasswordView.as_view(), name='change_password'),
]
