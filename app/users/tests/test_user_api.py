"""Tests for user API"""
from django.test import TestCase
from django.contrib.auth import get_user_model
from django.urls import reverse

from rest_framework.test import APIClient
from rest_framework import status

CREATE_USER_URL = reverse('users:register_user')
TOKEN_URL = reverse('users:login_user')
REFRESH_TOKEN_URL = reverse('users:token_refresh')


def create_user(**params):
    """Create and return new user"""
    return get_user_model().objects.create_user(**params)


class PublicUserApiTests(TestCase):
    """Tests for the features that do not need auth"""

    def setUp(self) -> None:
        self.client = APIClient()

    def test_create_user_success(self):
        """Check if user was created successfully"""
        payload = {
            'email': 'test@example.com',
            'password': 'Testowe123!',
        }
        res = self.client.post(CREATE_USER_URL, payload)

        self.assertEqual(res.status_code, status.HTTP_201_CREATED)
        user = get_user_model().objects.get(email=payload['email'])
        self.assertTrue(user.check_password(payload['password']))
        self.assertNotIn('password', res.data)

    def test_user_with_email_exists_error(self):
        """Check if API return error when user with such email exists"""
        payload = {
            'email': 'test@example.com',
            'password': 'Testowe123!',
        }
        create_user(**payload)

        res = self.client.post(CREATE_USER_URL, payload)
        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)

        second_user_payload = {
            'email': 'TesT@example.com',
            'password': 'Testowe123!',
        }

        res = self.client.post(CREATE_USER_URL, payload)
        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)

    def test_password_too_short_error(self):
        """Check if API return error when password has less than 8 chars"""
        payload = {
            'email': 'test@example.com',
            'password': 'short',
        }
        res = self.client.post(CREATE_USER_URL, payload)
        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)
        user_exists = get_user_model().objects.filter(
            email=payload['email']
        ).exists()
        self.assertFalse(user_exists)

    def test_email_case_insensitive_during_login(self):
        """Check if email is case-insensitive during login user"""
        logins = [
            'user@example.com',
            'User@example.com',
            'USER@example.com',
        ]
        password = 'Testowe123!'

        register_payload = {
            'email': 'user@example.com',
            'password': password,
        }

        create_user(**register_payload)

        for login in logins:
            payload = {
                'email': login,
                'password': password,
            }

            res = self.client.post(TOKEN_URL, payload)
            self.assertEqual(res.status_code, status.HTTP_200_OK)
            self.assertIn('access', res.data)
            self.assertIn('refresh', res.data)

    def test_create_token_for_user(self):
        """Check if token was generated for valid credentials"""
        payload = {
            'email': 'test@example.com',
            'password': 'Testowe123!',
        }
        create_user(**payload)
        res = self.client.post(TOKEN_URL, payload)

        self.assertEqual(res.status_code, status.HTTP_200_OK)
        self.assertIn('access', res.data)
        self.assertIn('refresh', res.data)

    def test_refresh_token(self):
        """Check if token is refreshed after passing valid access token"""
        payload = {
            'email': 'test@example.com',
            'password': 'Testowe123!',
        }
        create_user(**payload)

        login_res = self.client.post(TOKEN_URL, payload)

        refresh_payload = {
            'refresh': login_res.data['refresh'],
        }

        refresh_res = self.client.post(REFRESH_TOKEN_URL, refresh_payload)

        self.assertEqual(refresh_res.status_code, status.HTTP_200_OK)
        self.assertIn('access', refresh_res.data)
