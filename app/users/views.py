from django.http import QueryDict
from django.contrib.auth import update_session_auth_hash
from rest_framework import status
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.permissions import AllowAny
from rest_framework_simplejwt.views import TokenObtainPairView
from rest_framework.permissions import IsAuthenticated
from django_rest_passwordreset.views import (
    ResetPasswordRequestToken,
    ResetPasswordConfirm,
)

from .serializers import (
    RegisterUserSerializer,
    AuthTokenSerializer,
    CustomResetPasswordSerializer,
    ChangePasswordSerializer,
    CustomPasswordTokenSerializer,
)
from core.common.utils import send_email


class UserView(APIView):
    serializer_class = RegisterUserSerializer

    def post(self, request):
        serializer = RegisterUserSerializer(data=request.data)

        if isinstance(request.data, QueryDict):
            request.data._mutable = True

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)

        custom_errors = {}
        for k, v in serializer.errors.items():
            custom_errors[k] = list(map(lambda x: str(x).capitalize(), v))

        return Response(custom_errors, status=status.HTTP_400_BAD_REQUEST)


class AuthTokenView(TokenObtainPairView):
    permission_classes = (AllowAny, )
    serializer_class = AuthTokenSerializer

    def post(self, request, *args, **kwargs):
        if isinstance(request.data, QueryDict):
            request.data._mutable = True
        return super().post(request, *args, **kwargs)


class CustomResetPasswordView(ResetPasswordRequestToken):
    serializer_class = CustomResetPasswordSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        return super().post(request, *args, **kwargs)


class ChangePasswordView(APIView):
    permission_classes = [IsAuthenticated]
    serializer_class = ChangePasswordSerializer

    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        if serializer.is_valid():
            user = request.user
            if user.check_password(serializer.data.get('old_password')):
                user.set_password(serializer.data.get('password'))
                user.save()
                update_session_auth_hash(request, user)
                send_email(
                    user=user,
                    html_msg_name="password_reset_successful_confirmation.html",
                    email_subject="Password reset successfully",
                )
                return Response({'message': 'Password changed successfully'}, status=status.HTTP_200_OK)
            return Response({'error': 'Incorrect old password!'}, status=status.HTTP_400_BAD_REQUEST)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class CustomResetPasswordConfirmView(ResetPasswordConfirm):
    serializer_class = CustomPasswordTokenSerializer
