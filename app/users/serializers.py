from django.contrib.auth import get_user_model
from django_rest_passwordreset.serializers import PasswordTokenSerializer
from rest_framework import serializers
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer

from .models import User
from .validators import validate_password_strength, validate_password_match


class RegisterUserSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ['email', 'password']
        extra_kwargs = {'password': {'write_only': True}}

    @staticmethod
    def validate_password(value):
        validate_password_strength(value)
        return value

    def to_internal_value(self, data):
        if 'email' in data:
            data['email'] = data['email'].lower()
        return super().to_internal_value(data)

    def create(self, validated_data):
        return get_user_model().objects.create_user(**validated_data)


class AuthTokenSerializer(TokenObtainPairSerializer):

    def to_internal_value(self, data):
        if 'email' in data:
            data['email'] = data['email'].lower()
        return super().to_internal_value(data)

    @classmethod
    def get_token(cls, user):
        token = super(TokenObtainPairSerializer, cls).get_token(user)
        token['email'] = user.email
        return token


class CustomResetPasswordSerializer(serializers.Serializer):
    email = serializers.EmailField(required=True)
    url = serializers.URLField(required=True)


class ChangePasswordSerializer(serializers.Serializer):
    old_password = serializers.CharField(required=True)
    password = serializers.CharField(
        required=True,
        validators=[validate_password_strength],
    )
    password_confirmation = serializers.CharField(required=True)

    def validate(self, attrs):
        return validate_password_match(attrs)


class CustomPasswordTokenSerializer(PasswordTokenSerializer):
    password = serializers.CharField(
        required=True,
        validators=[validate_password_strength],
    )
    password_confirmation = serializers.CharField(required=True)

    def validate(self, attrs):
        attrs = super().validate(attrs)
        return validate_password_match(attrs)
