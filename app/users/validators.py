from django.contrib.auth.password_validation import validate_password
from django.core.exceptions import ValidationError
from rest_framework import serializers


def validate_password_strength(value: str) -> None:
    try:
        validate_password(value)
    except ValidationError as e:
        raise serializers.ValidationError(e.messages)


def validate_password_match(attrs):
    if attrs['password'] != attrs['password_confirmation']:
        raise serializers.ValidationError("Passwords do not match.")
    return attrs
